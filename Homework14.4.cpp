#include <iostream>
#include <string>

int main()
{
    std::string OutputString = "Hello World!";

    std::cout << "String = " << OutputString << std::endl;

    std::cout << "String length = " << OutputString.length() << std::endl;

    std::cout << "First symbol = " << OutputString[0] << std::endl;

    std::cout << "Last symbol = " << OutputString[OutputString.length() - 1] << std::endl;

    return 0;
}